/*
    This file is part of the Okteta Kasten module, made within the KDE community.

    Copyright 2009-2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "bytearraystreamencoderconfigeditorfactoryfactory.hpp"

// lib
#include "streamencoder/sourcecode//bytearraysourcecodestreamencoderconfigeditorfactory.hpp"
#include "streamencoder/values/bytearrayvaluesstreamencoderconfigeditorfactory.hpp"
#include "streamencoder/base32/bytearraybase32streamencoderconfigeditorfactory.hpp"
#include "streamencoder/uuencoding/bytearrayuuencodingstreamencoderconfigeditorfactory.hpp"
#include "streamencoder/xxencoding/bytearrayxxencodingstreamencoderconfigeditorfactory.hpp"
#include "streamencoder/srec/bytearraysrecstreamencoderconfigeditorfactory.hpp"
#include "streamencoder/ihex/bytearrayihexstreamencoderconfigeditorfactory.hpp"
//// NEWBYTEARRAYSTREAMENCODERCONFIGEDITORFACTORY(start)
//// Here add the name of your header file of your streamencoder,
//// e.g.
//// #include "my_bytearraystreamencoder.hpp"
//// NEWBYTEARRAYSTREAMENCODERCONFIGEDITORFACTORY(end)
// Qt
#include <QVector>

namespace Kasten {

QVector<AbstractModelStreamEncoderConfigEditorFactory*> ByteArrayStreamEncoderConfigEditorFactoryFactory::createFactorys()
{
    const QVector<AbstractModelStreamEncoderConfigEditorFactory*> result {
        new ByteArraySourceCodeStreamEncoderConfigEditorFactory(),
        new ByteArrayValuesStreamEncoderConfigEditorFactory(),
        new ByteArrayBase32StreamEncoderConfigEditorFactory(),
        new ByteArraySRecStreamEncoderConfigEditorFactory(),
        new ByteArrayIHexStreamEncoderConfigEditorFactory(),
        new ByteArrayUuencodingStreamEncoderConfigEditorFactory(),
        new ByteArrayXxencodingStreamEncoderConfigEditorFactory(),
//// NEWBYTEARRAYSTREAMENCODERCONFIGEDITORFACTORY(start)
//// Here add the creation of an object of your streamencoder class and add it to the list,
//// e.g.
////         new My_ByteArrayStreamEncoderConfigEditorFactory(),
//// NEWBYTEARRAYSTREAMENCODERCONFIGEDITORFACTORY(end)
    };
    return result;
}

}
