/*
    This file is part of the Okteta Kasten module, made within the KDE community.

    Copyright 2006-2009 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KASTEN_SEARCHCONTROLLER_HPP
#define KASTEN_SEARCHCONTROLLER_HPP

// controller
#include "searchuserqueryable.hpp"
// libfinddialog
#include <kasten/okteta/finddirection.hpp>
// Kasten gui
#include <Kasten/AbstractXmlGuiController>

class KXMLGUIClient;
class QAction;
class QWidget;

namespace Kasten {

class SearchDialog;
class SearchTool;

class SearchController : public AbstractXmlGuiController
                       , public If::SearchUserQueryable
{
    Q_OBJECT

public:
    SearchController(KXMLGUIClient* guiClient, QWidget* parentWidget);
    ~SearchController() override;

public: // AbstractXmlGuiController API
    void setTargetModel(AbstractModel* model) override;

public: // SearchUserQueryable API
    bool queryContinue(FindDirection direction) const override;

private:
    void showDialog(FindDirection Direction);

private Q_SLOTS: // action slots
    void find();
    void findNext();
    void findPrevious();

    void onDataNotFound();

private:
    QWidget* mParentWidget;

    QAction* mFindAction;
    QAction* mFindNextAction;
    QAction* mFindPrevAction;

    SearchDialog* mSearchDialog = nullptr;
    SearchTool* mTool;
};

}

#endif
