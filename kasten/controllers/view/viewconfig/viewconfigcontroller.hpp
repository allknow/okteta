/*
    This file is part of the Okteta Kasten module, made within the KDE community.

    Copyright 2006-2010,2012 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KASTEN_VIEWCONFIGCONTROLLER_HPP
#define KASTEN_VIEWCONFIGCONTROLLER_HPP

// Kasten gui
#include <Kasten/AbstractXmlGuiController>

class KXMLGUIClient;
class KSelectAction;
class KToggleAction;
class QAction;

namespace Kasten {

class ByteArrayView;

class ViewConfigController : public AbstractXmlGuiController
{
    Q_OBJECT

public:
    explicit ViewConfigController(KXMLGUIClient* guiClient);

public: // AbstractXmlGuiController API
    void setTargetModel(AbstractModel* model) override;

private Q_SLOTS: // action slots
    void setValueCoding(int valueCoding);
    void setCharCoding(int valueCoding);
    void setShowsNonprinting(bool on);
    void showBytesPerLineDialog();
    void showBytesPerGroupDialog();
    void setLayoutStyle(int layoutStyle);
    void setOffsetCoding(int offsetCoding);
    void toggleOffsetColumn(bool on);
    void toggleValueCharColumns(int visibleColunms);

    void onOffsetColumnVisibleChanged(bool offsetColumnVisible);
    void onOffsetCodingChanged(int offsetCoding);
    void onShowsNonprintingChanged(bool showsNonprinting);
    void onValueCodingChanged(int valueCoding);
    void onCharCodecChanged(const QString& charCodecName);
    void onLayoutStyleChanged(int layoutStyle);
    void onVisibleByteArrayCodingsChanged(int visibleByteArrayCodings);

    void setBytesPerLine(int bytesPerLine);
    void setBytesPerGroup(int bytesPerGroup);

private:
    ByteArrayView* mByteArrayView = nullptr;

    // view menu
    KSelectAction* mCodingAction;
    KSelectAction* mEncodingAction;
    KToggleAction* mShowsNonprintingAction;
    QAction* mSetBytesPerLineAction;
    QAction* mSetBytesPerGroupAction;
    KSelectAction* mResizeStyleAction;
    KToggleAction* mShowOffsetColumnAction;
    KSelectAction* mOffsetCodingAction;
    KSelectAction* mToggleColumnsAction;
};

}

#endif
