/*
    This file is part of the Kasten Framework, made within the KDE community.

    Copyright 2009 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KASTEN_BOOKMARKSTOOL_HPP
#define KASTEN_BOOKMARKSTOOL_HPP

// Kasten core
#include <Kasten/AbstractTool>
// Okteta core
#include <Okteta/Address>

namespace Okteta {
class Bookmarkable;
class Bookmark;
class AbstractByteArrayModel;
}
template <class T> class QVector;

namespace Kasten {

class ByteArrayView;

class BookmarksTool : public AbstractTool
{
    Q_OBJECT

private:
    static constexpr int MaxBookmarkNameSize = 40;

public:
    BookmarksTool();
    ~BookmarksTool() override;

public:
    bool hasBookmarks() const;
    bool canCreateBookmark() const;
    const Okteta::Bookmark& bookmarkAt(unsigned int index) const;
    int indexOf(const Okteta::Bookmark& bookmark) const;
    unsigned int bookmarksCount() const;
    int offsetCoding() const;

public: // AbstractTool API
//     virtual AbstractModel* targetModel() const;
    QString title() const override;

    void setTargetModel(AbstractModel* model) override;

public:
    Okteta::Bookmark createBookmark();
    void gotoBookmark(const Okteta::Bookmark& bookmark);
    void setBookmarkName(unsigned int bookmarkIndex, const QString& name);
    void deleteBookmarks(const QVector<Okteta::Bookmark>& bookmarks);

Q_SIGNALS:
    void hasBookmarksChanged(bool hasBookmarks);
    void bookmarksAdded(const QVector<Okteta::Bookmark>& bookmarks);
    void bookmarksRemoved(const QVector<Okteta::Bookmark>& bookmarks);
    void bookmarksModified(const QVector<int>& indizes);
    void canCreateBookmarkChanged(bool canCreateBookmark);
    void offsetCodingChanged(int offsetCoding);

private Q_SLOTS:
    void onCursorPositionChanged(Okteta::Address newPosition);
    void onBookmarksModified();

private: // sources
    ByteArrayView* mByteArrayView = nullptr;
    Okteta::AbstractByteArrayModel* mByteArray = nullptr;
    Okteta::Bookmarkable* mBookmarks = nullptr;

    bool mCanCreateBookmark = false;
};

}

#endif
