/*
 *   This file is part of the Okteta Kasten Framework, made within the KDE community.
 *
 *   Copyright 2010 Alex Richardson <alex.richardson@gmx.de>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "unsignedbitfielddatainformation.hpp"

#include <KLocalizedString>

#include <QScriptValue>

#include "../poddecoder/typeeditors/uintspinbox.hpp"
#include "../uintdatainformation.hpp"

QString UnsignedBitfieldDataInformation::valueStringImpl() const
{
    Q_ASSERT(mWasAbleToRead);
    return UIntDataInformationMethods<quint64>::staticValueString(mValue.value<quint64>());
}

QWidget* UnsignedBitfieldDataInformation::createEditWidget(QWidget* parent) const
{
    auto* ret = new UIntSpinBox(parent);
    ret->setBase(Kasten::StructureViewPreferences::unsignedDisplayBase());
    ret->setMaximum(mask());
    return ret;
}

QVariant UnsignedBitfieldDataInformation::dataFromWidget(const QWidget* w) const
{
    const auto* spin = qobject_cast<const UIntSpinBox*> (w);
    if (spin) {
        return spin->value();
    }
    return {};
}

void UnsignedBitfieldDataInformation::setWidgetData(QWidget* w) const
{
    auto* spin = qobject_cast<UIntSpinBox*> (w);
    if (spin) {
        spin->setValue(mValue.value<quint64>());
    }
}

QScriptValue UnsignedBitfieldDataInformation::valueAsQScriptValue() const
{
    if (width() <= 32) {
        return mValue.value<quint32>() & quint32(mask());  // 32 bit or less -> can be put in as value
    }

    return QString::number(mValue.value<quint64>());
}

QString UnsignedBitfieldDataInformation::typeNameImpl() const
{
    return i18ncp("Data type", "unsigned bitfield (%1 bit wide)",
                  "unsigned bitfield (%1 bits wide)", width());
}

AbstractBitfieldDataInformation::Type UnsignedBitfieldDataInformation::bitfieldType() const
{
    return AbstractBitfieldDataInformation::Type::Unsigned;
}
