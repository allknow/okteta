/*
    This file is part of the Kasten Framework, made within the KDE community.

    Copyright 2008 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KASTEN_PERSON_P_HPP
#define KASTEN_PERSON_P_HPP

#include "person.hpp"

// Qt
#include <QString>
#include <QIcon>

namespace Kasten {

class PersonPrivate : public QSharedData
{
public:
    PersonPrivate(const QString& name, const QIcon& faceIcon);
    PersonPrivate() = delete;

    ~PersonPrivate();

public:
    QString name() const;
    QIcon faceIcon() const;

private:
    QString mName;
    QIcon mFaceIcon;
};

inline PersonPrivate::PersonPrivate(const QString& name, const QIcon& faceIcon)
    : mName(name)
    , mFaceIcon(faceIcon)
{}

inline PersonPrivate::~PersonPrivate() = default;

inline QString PersonPrivate::name()   const { return mName; }
inline QIcon PersonPrivate::faceIcon() const { return mFaceIcon; }

}

#endif
