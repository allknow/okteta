/*
    This file is part of the Kasten Framework, made within the KDE community.

    Copyright 2008-2009,2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KASTEN_ABSTRACTFILESYSTEMSYNCWITHREMOTEJOB_P_HPP
#define KASTEN_ABSTRACTFILESYSTEMSYNCWITHREMOTEJOB_P_HPP

// library
#include "abstractfilesystemsyncwithremotejob.hpp"
#include <abstractsyncwithremotejob_p.hpp>

#include <QUrl>

namespace Kasten {

class AbstractFileSystemSyncWithRemoteJobPrivate : public AbstractSyncWithRemoteJobPrivate
{
public:
    AbstractFileSystemSyncWithRemoteJobPrivate(AbstractFileSystemSyncWithRemoteJob* parent,
                                               AbstractModelFileSystemSynchronizer* synchronizer,
                                               const QUrl& url, AbstractModelSynchronizer::ConnectOption option);
    AbstractFileSystemSyncWithRemoteJobPrivate() = delete;

    ~AbstractFileSystemSyncWithRemoteJobPrivate() override;

public: // KJob API
    void start();

protected:
    AbstractModelFileSystemSynchronizer* synchronizer() const;
    QFile* file() const;

protected:
    void completeSync(bool success);

protected: // slots
    void syncWithRemote();

protected:
    AbstractModelFileSystemSynchronizer* const mSynchronizer;
    const QUrl mUrl;
    const AbstractModelSynchronizer::ConnectOption mOption;
    QFile* mFile = nullptr;
    QString mWorkFilePath;
    QString mTempFilePath;

private:
    Q_DECLARE_PUBLIC(AbstractFileSystemSyncWithRemoteJob)
};

inline AbstractFileSystemSyncWithRemoteJobPrivate::AbstractFileSystemSyncWithRemoteJobPrivate(AbstractFileSystemSyncWithRemoteJob* parent,
                                                                                              AbstractModelFileSystemSynchronizer* synchronizer,
                                                                                              const QUrl& url, AbstractModelSynchronizer::ConnectOption option)
    : AbstractSyncWithRemoteJobPrivate(parent)
    , mSynchronizer(synchronizer)
    , mUrl(url)
    , mOption(option)
{}

inline AbstractFileSystemSyncWithRemoteJobPrivate::~AbstractFileSystemSyncWithRemoteJobPrivate() = default;

inline QFile* AbstractFileSystemSyncWithRemoteJobPrivate::file()     const { return mFile; }
// TODO: setup a notification system
inline AbstractModelFileSystemSynchronizer* AbstractFileSystemSyncWithRemoteJobPrivate::synchronizer() const
{
    return mSynchronizer;
}

inline void AbstractFileSystemSyncWithRemoteJobPrivate::start()
{
    Q_Q(AbstractFileSystemSyncWithRemoteJob);

    QMetaObject::invokeMethod(q, "syncWithRemote", Qt::QueuedConnection);
}

}

#endif
